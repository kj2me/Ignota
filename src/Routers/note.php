<?php
use Controllers\NoteController;
use Libs\Router;

Router::get('/', [NoteController::class, 'home']);
Router::post('/', [NoteController::class, 'create']);
Router::get('/{id}/{key}', [NoteController::class, 'show']);
