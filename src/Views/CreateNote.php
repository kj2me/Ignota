<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ignota - Notas encriptadas punto a punto</title>
    <style>
     form {
       display: grid;
       place-content: center;
       gap: 5px;
       max-height: 100vh;
       min-height: 50vh;
       text-align: center;
     }

     textarea {
       min-width: 80vw;
       max-width: 95vw;
       min-height: 50vh;
     }

     button {
       padding: 5px;
       font-size: 18px;
       cursor: pointer;
     }

     .config {
       display: grid;
       grid-template-columns: auto 100px;
       text-align: left;
       gap: 5px;
       justify-content: center;
     }

     legend {
       font-size: 15px;
       font-weight: bold;
       display: ;
     }
    </style>
  </head>
  <body>
    <form method="POST">
      <textarea name="note" placeholder="Escribe tu texto a encriptar"></textarea>
      <fieldset class="config">
        <legend>Configuración de expiración</legend>
        <label for="max_views">Visualizaciones máximas (0 = infinitas):</label>
        <input name="max_views" type="int" value="0">
        <label for="expiration">Tiempo de expiración (en minutos):</label>
        <input name="expiration" type="int" value="15">
      </fieldset>
      <button type="submit">Guardar</button>
    </form>
  </body>
</html>
