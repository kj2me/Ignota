<?php
namespace Models;

use Libs\Model;

class Note extends Model {
    public string $content;
    public ?int   $expire_at;
    public int    $max_views = 0;

    /**
     * Encripta la nota
     *
     * @return string
     */
    public function encrypt(): string
    {
        // Generamos una llave aleatoria de entre 12 a 32 caracteres
        $secretKey = substr(
            str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'),
            0,
            mt_rand(16, 32)
        );

        // Encriptamos la nota en AES256 usando la llave antes generada
        $this->content = openssl_encrypt(
            $this->content,
            'AES-256-CBC',
            $secretKey,
            OPENSSL_RAW_DATA,
            substr($secretKey, 0, 16)
        );

        // Devolvemos la llave
        return $secretKey;
    }

    /**
     * Desencripta la nota a la vez que revisa si ha expirado por tiempo o por visitas máximas
     *
     * @param string $secretKey
     *
     * @return void
     */
    public function decrypt(string $secretKey) : void
    {
        // Verificamos si la nota ha expirado
        if ($this->expire_at < time()) {
            $this->content = 'La nota se ha destruido por expiración.';
            $this->delete();
            return;
        }

        // Intentamos desencriptar.
        $content = openssl_decrypt(
            $this->content,
            'AES-256-CBC',
            $secretKey,
            OPENSSL_RAW_DATA,
            substr($secretKey, 0, 16)
        );

        // Verificamos si desencriptó correctamente
        if (is_string($content)) {

            // Verificamos si está configurado para infinitas vistas.
            if ($this->max_views == 0) {
                $this->content = $content;
                return;
            }

            // Reducimos el contador de vistas
            $this->max_views = $this->max_views - 1;
            $this->save();

            // Verificamos si el contador de vistas ha llegado a 0.
            if ($this->max_views <= 0) {
                $this->content = 'La nota se ha destruido según su límite de vistas.';
                $this->delete();
                return;
            }

            // Si no ha expirado, solo devolvemos el contenido
            $this->content = $content;
        } else {
            // Si no se pudo desencriptar colocamos un error
            $this->content = 'Llave incorrecta.';
        }

    }
}
