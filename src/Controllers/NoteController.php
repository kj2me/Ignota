<?php
namespace Controllers;

use Libs\Request;
use Libs\Router;
use Libs\View;
use Models\Note;

class NoteController {

    /**
     * Ruta de inicio / Creación de nota
     *
     * @param Request $request
     *
     * @return void
     */
    public static function home(Request $request): void
    {
        View::render('CreateNote');
    }

    /**
     * Recibe el formulario y crea con él la nota encriptada
     *
     * @param Request $request
     *
     * @return void
     */
    public static function create(Request $request): void
    {
        // Creamos una instancia de Note y le pasamos los datos del formulario
        $note            = new Note;
        $note->content   = $request->post->note;
        $note->max_views = $request->post->max_views;
        $note->expire_at = strtotime("+{$request->post->expiration} minutes");

        // Encriptamos los datos y luego los guardamos
        $secretKey = $note->encrypt();
        $note->save();

        // Redirigimos a la url final donde se verá la nota
        Router::redirect("/{$note->id}/{$secretKey}");
    }

    /**
     * Intenta mostrar la nota desencriptada.
     *
     * @param Request $request
     *
     * @return void
     */
    public static function show(Request $request): void
    {
        // Verificamos si el ID es un número
        if (!is_numeric($request->params->id)) {
            Router::defaultNotFound();
            return;
        }

        // Obtenemos la nota de la base de datos
        $note = Note::getById($request->params->id);

        // Verificamos si la nota existe
        if (is_null($note)) {
            Router::defaultNotFound();
            return;
        }

        // Desencriptamos la nota
        $note->decrypt($request->params->key);

        // Imprimimos en formato de texto plano el resultado del desencriptado
        $view = new View;
        $view->text($note->content);
    }
}
