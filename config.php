<?php
// Configuración de la base de datos
define('DB_TYPE', 'sqlite');
define('DB_HOST', 'localhost');
define('DB_NAME', 'my-db.sqlite');
define('DB_USER', '');
define('DB_PASS', '');

// Configuración del sitio
define('SITE_URL', '');

// Configuración avanzada
define('ROOT_DIR', __DIR__);
define('ROOT_CORE', ROOT_DIR.'/src');
